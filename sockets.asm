section .data
	starting:	db "starting",10,0
	len:	equ $-starting
	text:	db "Hello world!!",10,0
	len2:	equ $-text
	text3:	db "worng",10,0
	len3:	equ $-text3
section .bss
	password:	resb 8
section .text
global _start
_start:
		mov rdi,1
		mov rsi,starting
		mov rdx,len
		mov rax,1
		syscall;print starting can be ignored 
		mov rdi,2;man socket,python socket
		mov rsi,1
		xor rdx,rdx
		mov rax,41
		syscall;socket
		mov rdi,rax;man ip
		push 0;this is the ip part must be 32 bit cause 255.255.255.255 is (2^8)*4 ie 8 bits*4 is 32 bits but here i pushed 64 bits which is fine since this is last entry
		push word 0x3905;should be word otherwise won't work for ip cause number of ports is ~ 65000 ie 2^16 hence the word 
		push word 2;this should also be word 
		mov rsi,rsp
		mov rdx,16;sockaddr has to have a size of 16 bytes, 2 from AF Family and 14 data (http://man7.org/linux/man-pages/man2/bind.2.html) , the structure Format is (http://man7.org/linux/man-pages/man7/ip.7.html)
		mov rax,49
		syscall;bind
		xor rsi,rsi
		mov rax,50
		syscall;listen
		xor rdx,rdx
		mov rax,43
		syscall;accept
		mov rdi,rax
		mov rsi,text
		mov rdx,len2
		mov r10,0
		mov r8,0
		mov r9,0
		mov rax,44
		syscall;send to 
		mov rsi,password
		mov rdx,8
		mov rax,45
		syscall;recv from
		mov rax,1
		mov rdi,1
		mov rsi,password;password is the buffer to store the message
		mov rdx,8
		syscall;this is to print the password on to the screen this full step can be ignored 
		lea rax,[password]
		mov rbx,0x6e6968636173;this is the password
		mov rcx,0
		mov rdx,0xff;mask
back:	push rbx
		and rbx,rdx;masking
		cmp byte [rax],bl
		jnz wrong
		pop rbx
		shr rbx,8;moving by one byte ie letter by letter comparison 
		inc rax
		inc rcx
		cmp rcx,5;len(password)-1
		jnz back
		mov rax,60;after this the code can be replaced to do a remote shell  
		syscall

;enter the remaining code here ignore line 68 & 69 and do the rest 


wrong:	mov rax,1
		mov rdi,1
		mov rsi,text3
		mov rdx,len3
		syscall; print if it is worng can be ignored 
		mov rax,60
		syscall

;we can implement this in the remote shellcode as an authentication if in case you are doing this for ctf ... nmap will show open port nd ppl can just use nc directly this adds authentication to it 